package net.suteren.nangucomments.domain;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;

/**
 * Entity of comment.
 */
@Data
@Document(indexName = "comments", type = "comment")
public class Comment {
    /**
     * ID of comment.
     */
    @Id
    private String id;
    /**
     * Author of comment.
     * Only author identified by his token is allowed to modify comment entity.
     */
    private Author author;
    /**
     * Content of message.
     */
    private String content;
}

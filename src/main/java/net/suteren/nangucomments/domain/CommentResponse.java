package net.suteren.nangucomments.domain;

import lombok.Data;

/**
 * Entity of comment for response to outer world.
 * No token and author ID are presented outside and structure is flat.
 */
@Data
public class CommentResponse {
    /**
     * ID of comment.
     */
    private String id;
    /**
     * Name of author.
     */
    private String author;
    /**
     * Content of message.
     */
    private String content;
}

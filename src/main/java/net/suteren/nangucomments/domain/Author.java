package net.suteren.nangucomments.domain;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;

/**
 * Entity of author.
 */
@Data
@Document(indexName = "authors", type = "author")
public class Author {
    /**
     * ID of author.
     */
    @Id
    private String id;
    /**
     * Name of author.
     */
    private String name;
    /**
     * Security token to allow author modify his content.
     * In real life it should be replaced by user management, but it's too huge to do in this example.
     */
    private String token;
}

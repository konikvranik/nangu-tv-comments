package net.suteren.nangucomments.dao;

import net.suteren.nangucomments.domain.Comment;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

/**
 * Comments repository template for elastic search.
 */

@Repository
public interface CommentElasticsearchRepository extends ElasticsearchRepository<Comment, String>, CommentRepository {

}

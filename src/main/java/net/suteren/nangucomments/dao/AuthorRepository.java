package net.suteren.nangucomments.dao;

import net.suteren.nangucomments.domain.Author;
import org.springframework.data.repository.CrudRepository;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * General authors repository template.
 * Declare all public methods here.
 */
public interface AuthorRepository extends CrudRepository<Author, String> {

}

package net.suteren.nangucomments.dao;

import net.suteren.nangucomments.domain.Author;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

/**
 * Authors repository template for elastic search.
 */
@Repository
public interface AuthorElasticsearchRepository extends ElasticsearchRepository<Author, String>, AuthorRepository {


}

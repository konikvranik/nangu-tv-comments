package net.suteren.nangucomments.dao;

import net.suteren.nangucomments.domain.Comment;
import org.springframework.data.repository.CrudRepository;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * General comments repository template.
 * Declare all public methods here.
 */
public interface CommentRepository extends CrudRepository<Comment, String> {
    List<Comment> findByAuthorId(@NotNull String name);
    List<Comment> findByAuthorNameAndContent(String authorName, String comment);
    List<Comment> findByAuthorName(String authorName);
    List<Comment> findByContent(String comment);

}

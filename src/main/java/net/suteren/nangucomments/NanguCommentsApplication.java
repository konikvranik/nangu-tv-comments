package net.suteren.nangucomments;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NanguCommentsApplication {

	public static void main(String[] args) {
		SpringApplication.run(NanguCommentsApplication.class, args);
	}

}

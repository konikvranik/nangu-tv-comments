package net.suteren.nangucomments.rest;

import net.suteren.nangucomments.domain.Comment;
import net.suteren.nangucomments.domain.CommentResponse;
import net.suteren.nangucomments.logic.CommentService;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;
import java.util.List;
import java.util.MissingResourceException;
import java.util.Optional;

import static java.util.stream.Collectors.toList;

/**
 * API of the application.
 */
@RestController
public class CommentsController {

    private final CommentService service;

    public CommentsController(CommentService service) {
        this.service = service;
    }

    /**
     * Get comments by author name, part of content, combination of both or all comments if none is specified.
     * No token infomration should be presented here.
     *
     * @param author  Author name to filter comments by. Ignored if blank or null.
     * @param content Content of message to filter by. Ignored if blank or null.
     * @return List of found comments
     */
    @GetMapping("/comments")
    public List<CommentResponse> getComments(@Null @RequestParam(value = "author", required = false) String author,
                                             @Null @RequestParam(value = "content", required = false) String content) {
        return service.getComments(author, content).stream().map(this::toResponse).collect(toList());

    }

    /**
     * Get single comment by ID.
     *
     * @param id ID of comment to return
     * @return Found comment or return 404 - not found
     */
    @GetMapping("/comment/{id}")
    public CommentResponse getComment(@NotNull @PathVariable("id") String id) {
        return Optional.ofNullable(service.getComment(id))
                .map(this::toResponse)
                .orElseThrow(() -> new MissingResourceException(null, Comment.class.getName(), id));
    }

    /**
     * Create new comment. Either authorId or authorName must be specified. If authorId is not present,
     * new Author with provided name will be created. Otherwise use existing author.
     *
     * @param authorId   ID of existing author
     * @param authorName Name of new author
     * @param content    Content of the message
     * @return Entity of new created comment
     */
    @PostMapping("/comment")
    public Comment createComment(@Null @RequestParam(value = "authorId", required = false) String authorId,
                                 @Null @RequestParam(value = "authorName", required = false) String authorName,
                                 @NotNull @RequestBody String content) {
        return service.createComment(authorId, authorName, content);

    }

    /**
     * Update existing comment. Valid author's token mus be provided,
     * otherwise 403 - unauthorized will be returned.
     *
     * @param id      ID of the comment to update
     * @param token   Valid auth token of comment's owner
     * @param content New content the message should be replaced with
     * @throws IllegalAccessException return 403 - unauthorized if thrown
     */
    @PutMapping("/comment/{id}")
    public void updateComment(@NotNull @PathVariable("id") String id,
                              @NotNull @RequestHeader(value = "token") String token,
                              @NotNull @RequestBody String content) throws IllegalAccessException {
        service.updateComment(id, content, token);
    }

    /**
     * Delete existing comment. Valid author's token mus be provided,
     * otherwise 403 - unauthorized will be returned.
     *
     * @param id    ID of the comment to delete
     * @param token Valid auth token of comment's owner
     * @throws IllegalAccessException return 403 - unauthorized if thrown
     */
    @DeleteMapping("/comment/{id}")
    public void deleteComment(@NotNull @PathVariable("id") String id,
                              @Null @RequestHeader(value = "token", required = false) String token) throws IllegalAccessException {
        service.deleteComment(id, token);
    }

    private CommentResponse toResponse(Comment comment) {
        CommentResponse result = new CommentResponse();
        result.setId(comment.getId());
        result.setContent(comment.getContent());
        result.setAuthor(comment.getAuthor().getName());
        return result;
    }

    @ExceptionHandler(MissingResourceException.class)
    public ResponseEntity<Object> onError(MissingResourceException ex) {
        return exceptionHandler(ex, HttpStatus.NOT_FOUND);

    }

    @ExceptionHandler(IllegalAccessException.class)
    public ResponseEntity<Object> onError(IllegalAccessException ex) {
        return exceptionHandler(ex, HttpStatus.FORBIDDEN);

    }

    @ExceptionHandler(IllegalArgumentException.class)
    public ResponseEntity<Object> onError(IllegalArgumentException ex) {
        return exceptionHandler(ex, HttpStatus.BAD_REQUEST);

    }

    public ResponseEntity<Object> exceptionHandler(Exception ex, HttpStatus status) {
        return new ResponseEntity<>(ex.getMessage(), new HttpHeaders(), status);

    }

}

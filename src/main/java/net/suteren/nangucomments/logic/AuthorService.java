package net.suteren.nangucomments.logic;

import net.suteren.nangucomments.dao.AuthorRepository;
import net.suteren.nangucomments.domain.Author;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.elasticsearch.core.ElasticsearchOperations;
import org.springframework.stereotype.Service;

import java.util.UUID;

/**
 * Service manipulating authors.
 */
@Service
public class AuthorService {

    private final AuthorRepository authorRepository;

    public AuthorService(@Qualifier("authorElasticsearchRepository") AuthorRepository authorRepository) {
        this.authorRepository = authorRepository;
    }

    /**
     * Get single author by ID.
     *
     * @param id ID of author to obtain.
     * @return Author entity
     */
    public Author getAuthor(String id) {
        return authorRepository.findById(id).orElse(null);
    }

    /**
     * Create new author.
     *
     * @param authorName Name of the author
     * @return Created entity of author wit ID and authentication token.
     */
    public Author createAuthor(String authorName) {
        Author author = new Author();
        author.setName(authorName);
        author.setToken(UUID.randomUUID().toString());
        return authorRepository.save(author);
    }

    /**
     * Delete author by ID.
     *
     * @param id ID of author to be deleted.
     */
    public void deleteAuthor(String id) {
        Author author = getAuthor(id);
        authorRepository.delete(author);
    }
}

package net.suteren.nangucomments.logic;

import net.suteren.nangucomments.dao.CommentRepository;
import net.suteren.nangucomments.domain.Author;
import net.suteren.nangucomments.domain.Comment;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.elasticsearch.core.ElasticsearchOperations;
import org.springframework.stereotype.Service;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;
import java.util.List;
import java.util.MissingResourceException;

import static com.fasterxml.jackson.core.util.BufferRecyclers.getJsonStringEncoder;
import static java.nio.charset.StandardCharsets.UTF_8;

/**
 * Service manipulating comments.
 */
@Service
public class CommentService {

    private final CommentRepository commentRepository;
    private final AuthorService authorService;

    public CommentService(@Qualifier("commentElasticsearchRepository") CommentRepository repository,
                          AuthorService authorService) {
        this.commentRepository = repository;
        this.authorService = authorService;
    }

    /**
     * Get comments by author name, part of content, combination of both or all comments if none is specified.
     *
     * @param authorName Author name to filter comments by. Ignored if blank or null.
     * @param content    Content of message to filter by. Ignored if blank or null.
     * @return List of found comments
     */
    public List<Comment> getComments(@Null String authorName, @Null String content) {

        if (authorName == null) {
            return commentRepository.findByContent(content);
        } else if (content == null) {
            return commentRepository.findByAuthorName(authorName);
        } else {
            return commentRepository.findByAuthorNameAndContent(jsonQuote(authorName), jsonQuote(content));
        }
    }

    private String jsonQuote(@Null String stringToQuote) {
        return stringToQuote == null ? null : new String(getJsonStringEncoder().quoteAsUTF8(stringToQuote), UTF_8);
    }

    /**
     * Get single comment by ID.
     *
     * @param id ID of comment to get
     * @return Comment of specified ID
     */
    public Comment getComment(@NotNull String id) {
        return commentRepository.findById(id).orElse(null);
    }

    /**
     * Create new comment. Either authorId or authorName must be specified. If authorId is not present,
     * new Author with provided name will be created. Otherwise use existing author.
     *
     * @param authorId   ID of existing author
     * @param authorName Name of new author
     * @param content    Content of the message
     * @return Entity of new created comment
     */
    public Comment createComment(@Null String authorId, @Null String authorName, @NotNull String content) {
        if (isBlank(authorId) == isBlank(authorName)) {
            throw new IllegalArgumentException("Exactly one of authorName or AuthorId must be fulfilled");
        }
        Author author;
        if (isBlank(authorId)) {
            author = authorService.createAuthor(authorName);
            if (author == null) {
                throw new IllegalArgumentException("Failed to create author");
            }
        } else {
            author = authorService.getAuthor(authorId);
            if (author == null) {
                throw new IllegalArgumentException("No such Author ID in repository.");
            }
        }

        Comment comment = new Comment();
        comment.setAuthor(author);
        comment.setContent(content);
        commentRepository.save(comment);
        return comment;
    }

    /**
     * Update existing comment. Valid author's token mus be provided, otherwise {@link IllegalAccessException} will be thrown.
     *
     * @param id      ID of the comment to update
     * @param content New content to replace message with
     * @param token   Valid auth token of comment's owner
     * @throws IllegalAccessException Thrown when token is not valid
     */
    public void updateComment(@NotNull String id, @NotNull String content, @NotNull String token) throws IllegalAccessException {
        Comment comment = getCommentSecure(id, token);
        comment.setContent(content);
        commentRepository.save(comment);
    }

    /**
     * Delete existing comment. Valid author's token mus be provided, otherwise {@link IllegalAccessException} will be thrown.
     *
     * @param id    ID of the comment to delete
     * @param token Valid auth token of comment's owner
     * @throws IllegalAccessException Thrown when token is not valid
     */
    public void deleteComment(@NotNull String id, @NotNull String token) throws IllegalAccessException {
        Comment comment = getCommentSecure(id, token);
        commentRepository.delete(comment);
        String authorId = comment.getAuthor().getId();
        if (getCommentsWithAuthor(authorId).isEmpty()) {
            authorService.deleteAuthor(authorId);
        }
    }

    private Comment getCommentSecure(@NotNull String id, @NotNull String token) throws IllegalAccessException {
        if (isBlank(token)) {
            throw new IllegalAccessException();
        }
        Comment comment = getComment(id);
        if (comment == null) {
            throw new MissingResourceException(null, Comment.class.getName(), id);
        } else if (!token.equals(comment.getAuthor().getToken())) {
            throw new IllegalAccessException();
        }
        return comment;
    }

    private List<Comment> getCommentsWithAuthor(@NotNull String authorId) {
        if (isBlank(authorId)) {
            throw new IllegalArgumentException();
        }
        return commentRepository.findByAuthorId(authorId);
    }

    private boolean isBlank(String string) {
        return string == null || string.isBlank();
    }
}
